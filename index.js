var co = {};
var marker = {};
var circles = {};
var map;
var i = 0;
var isPaused = false;

function getLocation() {
  navigator.geolocation.getCurrentPosition(
    function (position) {
      co = position.coords;
      co = new google.maps.LatLng(co.latitude, co.longitude);
      var mapProp = {
        center: co,
        zoom: 15,
      };
      var labelforCircle = new google.maps.Marker({
        id: 1,
        position: google.maps.geometry.spherical.computeOffset(co, 0, i * 10),
        map: map,
        scaledSize: new google.maps.Size(0, 0),
        icon: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
        fillOpacity: 0,
      });

      map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

      var sniffer = new google.maps.Marker({
        label: { text: "Sniffer", color: "white" },
        position: co,
        map: map,
      });

      var circle = new google.maps.Circle({
        id: 0,
        strokeColor: 0x1000000,
        center: co,
        map: map,
        fillOpacity: 0,
        radius: 0,
      });
    },
    function errorCallback(error) {
      console.warn(`ERROR(${error.code}): ${error.message}`);
    },
    {
      timeout: 5000,
    }
  );
}

window.setInterval(function () {
  if (!isPaused) {
    i += 1;
    var xmlHttp = new XMLHttpRequest();

    http: xmlHttp.open("GET", "http://127.0.0.1:5000/", false);

    xmlHttp.send(null);
    res = JSON.parse(xmlHttp.responseText);

    var obj = res.elem;

    if (markerNotExists(obj.ssid)) {
      var color =
        "#" + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);
      var circle = new google.maps.Circle({
        id: obj.ssid,
        strokeColor: color,
        center: co,
        map: map,
        fillOpacity: 0,
        radius: obj.radius,
      });
      var labelforCircle = new google.maps.Marker({
        id: obj.ssid,
        position: google.maps.geometry.spherical.computeOffset(
          co,
          obj.radius,
          i * 10
        ),
        label: { text: obj.ssid, color: "white" },
        map: map,
        scaledSize: new google.maps.Size(70, 70),
        icon: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
      });
      circle.setMap(map);
      labelforCircle.setMap(map);
      marker[obj.ssid] = labelforCircle;
      circles[obj.ssid] = circle;
      document.getElementById("ssidList").innerHTML =
        document.getElementById("ssidList").innerHTML +
        "<p id='ssids' style='background-color: rgb(240, 240, 240)'>" +
        obj.ssid +
        "<p>";
    }
  }
}, 1000);

function markerNotExists(ssid) {
  if (marker[ssid] == undefined) {
    return true;
  }
  return false;
}

function startSniffing() {
  for (const [key, value] of Object.entries(marker)) {
    value.setVisible(true);
  }
  for (const [key, value] of Object.entries(circles)) {
    value.setOptions({ strokeOpacity: 1 });
  }
  document.getElementById("button-show").style.display = "none";
  isPaused = false;
}

function hideMarker(ssid) {
  for (const [key, value] of Object.entries(marker)) {
    value.setVisible(true);
    if (key != ssid) {
      value.setVisible(false);
    }
  }
  for (const [key, value] of Object.entries(circles)) {
    value.setOptions({ strokeOpacity: 1 });
    if (key != ssid) {
      value.setOptions({ strokeOpacity: 0 });
    }
  }
}

document.querySelector("#ssidList").addEventListener("click", function (event) {
  if (event.target.id === "ssids") {
    hideMarker(event.target.innerHTML);
    document.getElementById("button-show").style.display = "block";
    isPaused = true;
  }
});
