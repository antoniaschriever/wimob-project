from flask import Flask
from flask_cors import CORS, cross_origin
import json

app = Flask(__name__)
cors = CORS(app, resources={
            r"/*": {"origins":  "http://localhost:8000" "*"}}, support_credentials=True)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['SECRET_KEY'] = '478njdk39dk3kdn39402'


@app.route('/')
@cross_origin(support_credentials=True)
def index():
    line = readLines("logs.txt")
    points = {}
    points["elem"] = calculateCoords(line)
    print(points)
    return points


def calculateCoords(line):
    js = json.loads(line)
    d = js["dist"]
    point = {}
    point["ssid"] = js["ssid"]
    point["radius"] = float(d*1000)
    return point


def readLines(path):
    logfile = open(path, 'r')
    lines = logfile.readlines()
    logfile.close()
    new_file = open(path, "w")
    try:
        lineD = lines[0]
        del lines[0]
        for line in lines:
            new_file.write(line)
        new_file.close()
        return lineD
    except:
        print("Index out of range, wait for new signals")


if __name__ == '__main__':
    app.run(debug=True)
