#!/usr/bin/python3
import json
from scapy.all import *
from scapy.config import conf
conf.ipv4_enabled = False

seen = []

def PacketHandler(pkt):
    if pkt.haslayer(Dot11Beacon) or pkt.haslayer(Dot11ProbeResp) or pkt.haslayer(Dot11ProbeReq):
        if str(pkt.info)[2:-1] not in seen and str(pkt.info)[2:-1] != "":
            seen.append(str(pkt.info)[2:-1])
            f = pkt[0][RadioTap].Channel
            dbm = -1*(pkt.dBm_AntSignal)
            c = 32.5
            d = math.pow(10, ((dbm-20*math.log(f, 10)-c)/20))
            packet = {}
            packet['dBm'] = pkt.dBm_AntSignal
            packet['ssid'] = str(pkt.info)[2:-1]
            packet['dist'] = d
            with open("logs.txt", 'a') as file:
                file.write(json.dumps(packet) + "\n")
            file.close()


with open('logs.txt', 'w'):
    pass

sniff(iface="en0", monitor=True, timeout=1000, prn=PacketHandler)
